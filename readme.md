# 使用方式

## runserver
1. install requirements from requirements.txt
2. python3 manage.py runserver
3. 已連上SQL

## 創建使用者
- http://{localhost}/users/


    input = {
        "name": str,
        "password": str,
        "email": str,
    }

## CRUD points
- 當創建完使用者後，會自動創建對應使用者ID的Points
- 使用 put/patch 進行點數給予以及使用
- http://{localhost}/points/{point_id}/ 
- 可至 http://{localhost}/points/ 查看 {point_id}