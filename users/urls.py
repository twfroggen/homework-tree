from . import views
from rest_framework import routers

router = routers.DefaultRouter()
app_name = "users"

router.register("users", views.UserViewSet, basename="users")

urlpatterns = []

urlpatterns += router.urls