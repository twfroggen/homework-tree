from django.shortcuts import render
from django.contrib.auth import get_user_model

from users.serializers import UserSerializer
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from rest_framework.viewsets import ModelViewSet

# Create your views here.
# @method_decorator([login_required, permission_required("users.change_user", raise_exception=True)], name="dispatch")
class UserViewSet(ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
