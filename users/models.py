from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
import uuid

class UserManager(BaseUserManager):
    def _create_user(self, name, email, password, is_superuser, **kwargs):
        if not email:
            raise ValueError("請輸入信箱")
        elif not password:
            raise ValueError("請輸入密碼")
        
        token = uuid.uuid4().hex

        user = self.model(name=name, email=email, token=token, is_superuser=is_superuser, **kwargs)
        user.set_password(password)
        user.save()

        return user

    def create_user(self, name, email, password, **kwargs):
        return self._create_user(name=name, email=email, password=password, is_superuser=0, **kwargs)

    def create_superuser(self, name, email, password, **kwargs):
        return self._create_user(name=name, email=email, password=password, is_superuser=1, **kwargs)

class User(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=10)
    email = models.CharField(max_length=30, unique=True, error_messages={'unique': '此信箱已被註冊'})
    is_active = models.BooleanField(default=True)

    # 登入後台 admin 需要的欄位
    is_staff = models.BooleanField(default=True)
    create_time = models.DateTimeField(auto_now_add=True)
    token = models.CharField(max_length=32, editable=False, unique=True, blank=True, default=uuid.uuid4().hex)

    class Meta: 
        db_table = "users"

    # 要驗證的唯一碼(PK)
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name"]

    objects = UserManager()

