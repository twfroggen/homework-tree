from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import get_user_model

class Points(models.Model):
    user = models.OneToOneField("users.User", on_delete=models.PROTECT)
    points = models.IntegerField(default=0)

    class Meta:
        db_table = "points"

@receiver(post_save, sender=get_user_model())
def create_point(sender, instance, created, **kwargs):
   if created:
       Points.objects.create(user=instance)