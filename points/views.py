from points.models import Points
from points.serializers import PointsSerializer

from rest_framework.viewsets import ModelViewSet

# @method_decorator([login_required, permission_required("points.change_points", raise_exception=True)], name="dispatch")
class PointsViewSet(ModelViewSet):
    queryset = Points.objects.all()
    serializer_class = PointsSerializer
