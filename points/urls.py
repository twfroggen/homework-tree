from . import views
from rest_framework import routers

router = routers.DefaultRouter()
app_name = "points"

router.register("points", views.PointsViewSet, basename="points")

urlpatterns = []

urlpatterns += router.urls